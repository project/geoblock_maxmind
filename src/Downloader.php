<?php

declare(strict_types=1);

namespace Drupal\geoblock_maxmind;

use Drupal\Core\Archiver\ArchiverInterface;
use Drupal\Core\Archiver\ArchiverManager;
use Drupal\Core\File\FileSystemInterface;

/**
 * A class to facilitate MaxMind database downloads.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class Downloader {

  /**
   * A file resource used to store the database archive after downloading.
   *
   * @var resource
   */
  protected $archive;

  /**
   * The path to the downloaded database archive.
   *
   * @var string
   */
  protected $archivePath;

  /**
   * The archiver plugin manager service.
   *
   * @var \Drupal\Core\Archiver\ArchiverManager
   */
  protected $archiverManager;

  /**
   * The destination file system path of the extracted MaxMind database.
   *
   * @var string
   */
  protected $destination;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The base directory to which any downloaded archive will be extracted.
   *
   * @var string
   */
  protected $sourceBase;

  /**
   * Constructs a Downloader object.
   *
   * @param \Drupal\Core\Archiver\ArchiverManager $archiver_manager
   *   The archiver plugin manager service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param string $destination
   *   The destination file system path of the extracted MaxMind database.
   *
   * @throws \Throwable
   *   If one or more prerequisites for downloading are not satisfied.
   */
  public function __construct(ArchiverManager $archiver_manager, FileSystemInterface $file_system, string $destination) {
    $this->archiverManager = $archiver_manager;
    $this->fileSystem = $file_system;
    $this->destination = $destination;

    $temp_dir = $this->fileSystem->getTempDirectory();

    if (FALSE === $this->archivePath = $this->fileSystem->tempnam($temp_dir, 'geoblock_maxmind_archive-')) {
      throw new \RuntimeException('Unable to create a temporary file used to store the downloaded archive');
    }

    $this->archivePath = $this->fileSystem->move($this->archivePath, "{$this->archivePath}.tar.gz", FileSystemInterface::EXISTS_REPLACE);

    if (!\is_resource($this->archive = \fopen($this->archivePath, 'w'))) {
      throw new \RuntimeException('Unable to open the temporary file for writing the downloaded archive');
    }

    $this->sourceBase = $temp_dir . '/geoblock_maxmind_output';
  }

  /**
   * Destructs a Downloader object.
   */
  public function __destruct() {
    if (\is_resource($this->archive)) {
      \fclose($this->archive);
    }

    if (\file_exists($this->archivePath)) {
      $this->fileSystem->unlink($this->archivePath);
    }
  }

  /**
   * Download & extract the MaxMind database in the archive at the supplied URL.
   *
   * The time at which the download was initiated is recorded to facilitate
   * periodic updates during cron runs.
   *
   * @param string $url
   *   The URL of the archive to download which contains a MaxMind database.
   *
   * @throws \Throwable
   *   If an unrecoverable error occurs.
   */
  public function download(string $url): void {
    $archiver = $this->downloadArchive($url);
    $database = $this->scanArchiveForDatabase($archiver);

    if (!$this->fileSystem->prepareDirectory($this->sourceBase, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      throw new \RuntimeException('Unable to create the temporary directory used to extract the MaxMind database');
    }

    try {
      $archiver->extract($this->sourceBase, [$database]);
      $this->fileSystem->copy("{$this->sourceBase}/{$database}", $this->destination, FileSystemInterface::EXISTS_REPLACE);
    }
    finally {
      $this->fileSystem->deleteRecursive($this->sourceBase);
    }
  }

  /**
   * Download the archive at the supplied URL.
   *
   * The remote archive will be downloaded to the internal temporary file, and
   * is only valid for the lifetime of this object.
   *
   * @param string $url
   *   The URL of the remote archive to download.
   *
   * @throws \RuntimeException
   *   If the remote archive is unable to be downloaded.
   *
   * @return \Drupal\Core\Archiver\ArchiverInterface
   *   An archiver for the downloaded archive.
   */
  protected function downloadArchive(string $url): ArchiverInterface {
    if (!\is_resource($remote = \fopen($url, 'r'))) {
      throw new \RuntimeException('Unable to open the remote archive for downloading');
    }

    try {
      if (\stream_copy_to_stream($remote, $this->archive) === FALSE || !\fflush($this->archive)) {
        throw new \RuntimeException('Unable to download the remote archive');
      }

      return $this->getArchiverForFile($this->archivePath);
    }
    finally {
      \fclose($remote);
    }
  }

  /**
   * Get an archiver for the supplied file path.
   *
   * @param string $path
   *   The path to the file for which to get an archiver.
   *
   * @return \Drupal\Core\Archiver\ArchiverInterface
   *   An archiver for the supplied file path.
   */
  protected function getArchiverForFile(string $path): ArchiverInterface {
    $archiver = $this->archiverManager->getInstance([
      'filepath' => $this->archivePath,
    ]);

    if (!$archiver instanceof ArchiverInterface) {
      throw new \RuntimeException('The archive is either corrupt or invalid');
    }

    return $archiver;
  }

  /**
   * Scan the downloaded archive for a MaxMind database file path.
   *
   * @param \Drupal\Core\Archiver\ArchiverInterface $archiver
   *   The archiver used to find a MaxMind database file path.
   *
   * @throws \RuntimeException
   *   If the archive does not contain a MaxMind database file.
   *
   * @return string|null
   *   The relative path to the MaxMind database within the archive.
   */
  protected function scanArchiveForDatabase(ArchiverInterface $archiver): string {
    $files = $archiver->listContents();

    $filter = function (string $file): bool {
      if (\preg_match('/.*\\.mmdb$/i', $file)) {
        return TRUE;
      }

      return FALSE;
    };

    if (FALSE === $file = \current(\array_filter($files, $filter))) {
      throw new \RuntimeException('The downloaded archive does not contain a MaxMind database file');
    }

    return $file;
  }

}
