<?php

declare(strict_types=1);

namespace Drupal\geoblock_maxmind\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Utility\Error;

use Drupal\geoblock_maxmind\Downloader;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a configuration form for this module.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The file system path of the MaxMind database.
   *
   * @var string
   */
  protected $databasePath;

  /**
   * The MaxMind database downloader service.
   *
   * @var \Drupal\geoblock_maxmind\Downloader
   */
  protected $downloader;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typed_config_manager, Downloader $downloader, StateInterface $state, string $database_path) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->databasePath = $database_path;
    $this->downloader = $downloader;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(current($this->getEditableConfigNames()));
    $form = parent::buildForm($form, $form_state);

    $form['download_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Download URL'),
      '#description' => $this->t('A permalink where a MaxMind database archive in *.tar.gz format can be downloaded. If no URL is provided, the MaxMind database must be stored at <code>@database_path</code> and should be manually updated in accordance with your license agreement.', [
        '@database_path' => $this->databasePath,
      ]),
      '#default_value' => $config->get('download_url'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('geoblock_maxmind.downloader'),
      $container->get('state'),
      $container->getParameter('geoblock_maxmind.database_path'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'geoblock_maxmind.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'geoblock_maxmind_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      if (!empty($url = $form_state->getValue('download_url'))) {
        $this->downloader->download($url);

        // Schedule the next update to occur at least 1 week in the future.
        $this->state->set('geoblock_maxmind.update_date', \strtotime('1 week'));
      }
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('download_url', $this->t('Unable to download a MaxMind database at the supplied URL. Check the recent log messages for more information.'));
      Error::logException($this->logger('geoblock_maxmind'), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(\current($this->getEditableConfigNames()));
    $config->setData($form_state->cleanValues()->getValues());
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
