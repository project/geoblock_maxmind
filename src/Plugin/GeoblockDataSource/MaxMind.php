<?php

declare(strict_types=1);

namespace Drupal\geoblock_maxmind\Plugin\GeoblockDataSource;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

use Drupal\geoblock\IPAddress;
use Drupal\geoblock\Plugin\GeoblockDataSourcePluginBase;

use LibraryMarket\MaxMind\Database\Reader;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A MaxMind geoblock data source plugin.
 *
 * Copyright (C) 2022  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @GeoblockDataSource(
 *   id = "maxmind",
 *   label = "MaxMind database file",
 * )
 */
class MaxMind extends GeoblockDataSourcePluginBase implements ContainerFactoryPluginInterface {

  /**
   * The database reader object.
   *
   * @var \LibraryMarket\MaxMind\Database\Reader|null
   */
  protected $reader;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, string $database_path) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    try {
      if (\is_file($database_path)) {
        $this->reader = new Reader($database_path);
      }
    }
    catch (\Throwable $e) {
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('geoblock_maxmind.database_path'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function locate(IPAddress $address): void {
    if ($this->reader && $address->isPublic()) {
      @[
        'country' => [
          'iso_code' => $country_code,
        ],
        'registered_country' => [
          'iso_code' => $registered_country_code,
        ],
      ] = $this->reader->searchForAddress($address->getAddress());

      if (isset($country_code)) {
        $address->setCountryCode($country_code);
      }

      if (isset($registered_country_code)) {
        $address->setRegisteredCountryCode($registered_country_code);
      }
    }
  }

}
